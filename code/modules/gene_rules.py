from pybiomart import Dataset
import cobra
import pandas as pd
from contextlib import closing
from urllib.request import urlopen
from joblib import Memory
from os import path
from pathlib import Path
from io import BytesIO
import json


wps_path = Path(__file__).absolute().parent.parent / "../data/wolfpsort_sasa.txt"
uniprot_entrez_table = Path(__file__).absolute().parent.parent / "../data/entrez_uniprot.csv"

mem = Memory(path.join(path.dirname(__file__), ".cache"))


def kegg_orthology_url(kegg_organism_id: str):
    return 'https://www.genome.jp/kegg-bin/download_htext?'\
           f'htext={kegg_organism_id}00001&format=json'


@mem.cache
def urlcache(url, data=None):
    """Cache download from URL."""
    with closing(urlopen(url, data)) as f:
        return f.read()


@mem.cache
def pdcache(url, *args, **kwargs):
    """Cache pandas.read_table."""
    return pd.read_table(BytesIO(urlcache(url)), *args, **kwargs)


@mem.cache
def jsoncache(url):
    """Cache json from url"""
    return json.loads(urlcache(url))


def parse_wolfpsort(table=wps_path):
    """
    Parse WolfPSort results, return dict

    >>> assert type(parse_wolfpsort()) == dict
    """
    p = dict()
    cd = dict(zip(['extr', 'E.R.', 'pero', 'lyso', 'cyto', 'nucl', 'mito', 'golg'],
                  ['e', 'r', 'x', 'l', 'c', 'n', 'm', 'g']))
    with open(table, 'r') as f:
        f.readline()
        for line in f:
            line = line.strip().split(' ')
            uniprot_id = line[0].split('|')[1]
            in_bigg = [key in cd.keys() for key in line[1::2]]
            compartments = [cd[c] for c, i in zip(line[1::2], in_bigg) if i]
            wps_scores = [float(score.replace(',', ''))
                          for score, i in zip(line[2::2], in_bigg) if i]
            p[uniprot_id] = dict(zip(compartments, wps_scores))
        f.close()
    return p


wps = parse_wolfpsort()


def load_ncbi_to_uniprot_table(table=uniprot_entrez_table):
    """
    Return dict for conversion between uniprot and entrez/ncbi

    >>> assert type(load_ncbi_to_uniprot_table()) == dict
    """
    d = dict()
    with open(table, "r") as f:
        f.readline()
        for line in f:
            uniprot, ncbi = line.replace("\n", "").split(",")
            if d.get(ncbi, False):
                d[ncbi].append(uniprot)
            else:
                d[ncbi] = [uniprot]
    return d


ncbi_uniprot = load_ncbi_to_uniprot_table()


def ncbi_to_uniprot(gene, conversion=None):
    """
    Problem: only one uniprot:ncbi...
    Perhaps the other way around is better..

    >>> assert ncbi_to_uniprot('106577463') == ['A0A1S3N6L5']
    """
    if not conversion:
        conversion = ncbi_uniprot
    return conversion.get(gene, list())


def lookup_wolfpsort(gene, conversion=None):
    """
    Look up gene in wolfpsort table, return {uniprot: {compartment: score}}
    """
    uniprots = ncbi_to_uniprot(gene, conversion)
    return {u: wps[u] for u in uniprots if u in wps}


def identify_compartment(gene):
    """
    Based on lookup_wolfpsort score, decide compartment.

    At present using compartment with highest sum, but could use other rules as well.

    >>> assert identify_compartment('106577463') == 'r'
    """
    wpscore = {u: score for u, score in lookup_wolfpsort(gene).items() if score}
    if not wpscore:
        return
    compartment_scores = pd.DataFrame(wpscore).T.fillna(0)
    return compartment_scores.sum().sort_values(ascending=False).index[0]


# def gpr_list(gpr):
#     """
#     Return list of high level OR relations (outside parantheses)
#     Failing for 'and ('

#     >>> gpr_list('100136522 or 100194623 or 100194624')
#     ['100136522', '100194623', '100194624']
#     >>> gpr_list('100136522 or ( 100194623 or 100194624 )')
#     ['100136522', '( 100194623 or 100194624 )']
#     >>> gpr_list('( 100136522 or 100194623 ) or ( 100194623 or 100194624 )')
#     ['( 100136522 or 100194623 )', '( 100194623 or 100194624 )']
#     >>> gpr_list('100136522 or 100194623 and 100194624')
#     ['100136522', '100194623 and 100194624']
#     >>> gpr_list('100136522 and ( 100194623 or 100194624 )')
#     ['100136522 and ( 100194623 or 100194624 )']
#     """
#     parantheses = list()
#     while '(' in gpr and ')' in gpr:
#         first, second = gpr.find('('), gpr.find(')')
#         expression = gpr[first:second+1]
#         parantheses.append(expression)
#         gpr = gpr.replace(expression, '')
#     return [g for g in gpr.split(' or ') if g] + parantheses


def update_gpr_with_duplicates(reaction, duplicates):
    """
    Substitute duplicated genes with 'it or the_duplicate'

    This is easy if we dont care about repeating the genes,
    but harder if we have to check whether the genes allready have an 'or' relation
    """
    gpr = reaction.gene_reaction_rule
    gene_list = reaction.genes
    for gene in gene_list:
        duplicate = duplicates.get(gene.id)
        if duplicate and duplicate not in gpr:
            gpr = gpr.replace(gene.id, f"( {gene.id} or {duplicate} )")
    reaction.gene_reaction_rule = gpr


def gene_dict(keys='hsapiens', vals='ssalar'):
    """
    Return a dict of {gene_names: pd.Series(ncbigene)} from between organisms

    TODO: add options for from and to ontology.
          at the moment, it only really works for hsa:sasa
    """
    data = Dataset(name=f'{vals}_gene_ensembl', host='http://www.ensembl.org')
    query1 = data.query(attributes=['entrezgene_id',
                                    'ensembl_gene_id']).dropna()
    query2 = data.query(attributes=['ensembl_gene_id',
                                    f'{keys}_homolog_associated_gene_name']).dropna()
    common_id = query1.columns.intersection(query2.columns)[0]
    query1 = query1.set_index(common_id)
    query2 = query2.set_index(common_id)
    df = pd.merge(query1, query2, left_index=True, right_index=True).dropna()
    val_id = query1.columns[[e not in query2 for e in query1]][0]
    key_id = query2.columns[[e not in query1 for e in query2]][0]
    df[val_id] = df[val_id].astype(int).astype(str)
    key_genes = df[key_id].unique()
    d = {key.replace('MT-', ''): df[val_id].loc[df[key_id] == key].to_list()
         for key in key_genes}
    return d


def remove_non_converted(gpr_list, conversion):
    """
    Remove genes if they are among conversion keys
    """
    return [e for e in gpr_list if e in conversion]


def convert_gpr(gpr: str, conversion: dict):
    """
    Convert gpr to orthologous genes from conversion.

    If multiple orthologs, use or between duplicates.

    Examples:

    1738_AT1 in human maps to two genes in Atlantic salmon.

    >>> conversion = {'26_AT1': ['106590278'],
    ...               '314_AT1': ['106601268', '100194731']}
    >>> convert_gpr('( 26_AT1 and 314_AT1 )', conversion)
    '( 106590278 and ( 106601268 or 100194731 ) )'
    """
    gpr_list = gpr.split(' ')
    elements = list()
    for i, element in enumerate(gpr_list):
        if element in ['(', ')', 'or', 'and']:
            elements += [element]
            gpr_list[i] = element
            continue
        new_val = conversion.get(element)
        if not new_val:
            continue
        if len(new_val) > 1:
            new_element = f"( {' or '.join(new_val)} )"
        else:
            new_element = ' or '.join(new_val)
        gpr_list[i] = new_element
        elements += [new_element]
    gpr_list = remove_non_converted(gpr_list, elements)
    gpr_str = ' '.join(gpr_list)
    return prune_gpr(gpr_str)


def prune_gpr(gpr):
    """
    Remove lonely boolean operatorts and parantheses

    >>> prune_gpr('123 or or or')
    '123'
    >>> prune_gpr('( 123 )')
    '123'
    """
    gpr = remove_short_parantheses(gpr)
    gpr = trim_logical_ends(gpr)
    for o in ['or', 'and']:
        gpr_list = gpr.split(o)
        gpr = f' {o} '.join([e.strip() for e in gpr_list
                             if o not in e and len(e.replace(' ', ''))])
    if gpr.find('(') == gpr.find(')') + 1 - len(gpr):
        gpr = gpr[1:-1].strip()
    return gpr


def starts_or_ends_with(string: str, ends: tuple):
    """
    Check whether string starts or ends with any of ends

    >>> assert starts_or_ends_with('abc', ('a', 'b'))
    >>> assert starts_or_ends_with('abc', ('b', 'c'))
    """
    return string.endswith(ends) or string.startswith(ends)


def trim_logical_ends(string: str):
    operators = ('and', 'or')
    while starts_or_ends_with(string, operators):
        for operator in operators:
            if string.endswith(operator):
                string = string[:-len(operator) - 1].strip()
            if string.startswith(operator):
                string = string[len(operator):].strip()
    # for parantheses
    for bo, b in zip(['( and', '( or', 'and )', 'or )'], ['('] * 2 + [')'] * 2):
        string = string.replace(bo, b)
    return string


def find_all(s: str, character: str):
    """
    Return all positions of character in string

    >>> find_all('bing bong', 'b')
    [0, 5]
    """
    return [i for i, c in enumerate(s) if c == character]


def find_empty_parantheses(s: str):
    """
    Identify short parantheses: remove or, and, whitespace
    then find adjacent (), return which parantheses they are in the sequence.

    >>> find_empty_parantheses('( and )')
    [(0, 6)]
    >>> find_empty_parantheses('123 or ( and )')
    [(7, 13)]
    >>> find_empty_parantheses('( 123 or ( and ) or 321 )')
    [(9, 15)]
    """
    ori_start = find_all(s, '(')
    compact_string = s
    for replace_character in ['or', 'and', ' ']:
        compact_string = compact_string.replace(replace_character, '')
    compact_start = find_all(compact_string, '(')
    ori_end = find_all(s, ')')
    compact_end = find_all(compact_string, ')')
    parantheses = pd.DataFrame([[e + 1 == f for e in compact_start] for f in compact_end],
                               columns=ori_start, index=ori_end)
    remove = list()
    for end in parantheses.index:
        for start in parantheses.columns:
            if parantheses.loc[end, start]:
                remove.append((start, end))
    return remove


def remove_short_parantheses(s: str):
    """
    Remove short parantheses

    >>> remove_short_parantheses('123 or 321 or ( and )')
    '123 or 321 or '
    """
    remove = find_empty_parantheses(s)
    characters = [char for idx, char in enumerate(s)
                  if not any(start - 1 < idx <= end for start, end in remove)]
    return ''.join(characters).replace('  ', ' ')


def enzyme_subunit_gene(kegg_organism_id='sasa', wolfpsort=True):
    """
    Return a dict with {enzymes:{subunits: {genes}}},
    sorted into compartments if wolfpsort.
    """
    kegg_orthology = jsoncache(kegg_orthology_url(kegg_organism_id))
    ec_dict = dict()
    for category in kegg_orthology.get('children'):
        for subcategory in category.get('children'):
            for pathway in subcategory.get('children'):
                genes = pathway.get('children')
                if not genes:
                    continue
                for gene in genes:
                    entry = gene.get('name')
                    if '[EC:' not in entry:
                        continue
                    start, end = entry.split('\t')
                    startlist = start.split(' ')
                    gene_id = startlist.pop(0)
                    start = ' '.join(startlist)
                    g_id2, g_name = start.split(';') if ';' in start else (None, start)
                    endlist = end.split(';')
                    middle = endlist.pop(0)
                    end = ' '.join(endlist)
                    kegg_orthology, protein_id = middle.split(' ')
                    protein_name, ec_numbers = end.replace(']', '').split('[EC:')
                    d = {'gene_id': gene_id, 'name': g_name, 'gene_id2': g_id2,
                         'protein_name': protein_name, 'protein_id': protein_id}
                    if wolfpsort:
                        d['compartment'] = identify_compartment(gene_id)
                    for ec in ec_numbers.split(' '):
                        if ec_dict.get(ec):
                            if ec_dict[ec].get(kegg_orthology):
                                ec_dict[ec][kegg_orthology].update({gene_id: d})
                            else:
                                ec_dict[ec][kegg_orthology] = {gene_id: d}
                        else:
                            ec_dict[ec] = {kegg_orthology: {gene_id: d}}
    return ec_dict


def get_compartment_subunits(orthology_dict, ec, compartment):
    """Return subunits for ec number from the desired compartment"""
    subunit_dict = dict()
    for subunit, genes in orthology_dict[ec].items():
        subunit_dict[subunit] = {gene_id: gene for gene_id, gene in genes.items()
                                 if gene['compartment'] in compartment}
    return subunit_dict


def lookup_ec(ec: str, orthology_dict: dict, compartment={'c'}):
    """
    Look up entry for ec number in orthology dict

    >>> g1 = {'gene_id': '123', 'name': 'toy_gene', 'gene_id2': 'abc',
    ...       'protein_name': 'ABC', 'protein_id': 'p123', 'compartment': 'c'}
    >>> g2 = {'gene_id': '321', 'name': 'toy_gene2', 'gene_id2': 'def',
    ...       'protein_name': 'DEF', 'protein_id': 'p321', 'compartment': 'm'}
    >>> d = {'0.0.7': {'k1': {'123':g1, '321': g2}}}
    >>> lookup_ec('0.0.7', d, 'c')
    '123'
    """
    subunits = orthology_dict.get(ec)
    if not subunits:
        return False
    complex_list = list()
    for _, genes in subunits.items():
        genes = [gene for _, gene in genes.items() if gene['compartment'] in compartment]
        if not len(genes):
            continue
        start, end = ('( ', ' )') if len(subunits) > 1 and len(genes) > 1 else ('', '')
        complex_list.append(f"{start}{' or '.join([g['gene_id'] for g in genes])}{end}")
    return ' and '.join(complex_list)


def orthology_gene_rule(reaction: cobra.Reaction, orthology_dict: dict):
    """Return gene rule for reaction based on ec-code annotation and kegg orthology"""
    ec = reaction.annotation.get('ec-code')
    if not ec:
        return False
    if type(ec) == str:
        return lookup_ec(ec, orthology_dict, reaction.compartments)
    else:
        enzymes = list()
        for enzyme in ec:
            result = lookup_ec(enzyme, orthology_dict, reaction.compartments)
            enzymes += [result] if result else []
        if not enzymes:
            return False
        return f"( {' ) or ( '.join(enzymes)} )" if len(enzymes) > 1 else enzymes.pop()
