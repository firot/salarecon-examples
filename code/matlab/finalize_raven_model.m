clc; clear; close all;

%% Load and prepare RAVEN model
load('sasa_raven.mat');
model.comps = {'c'};
model.compNames = {'cytoplasm'};

%% Load WoLF PSORT scores
gss = parseScores('data/wolfpsort_sasa_genes.txt','wolf');

%% Use WoLF PSORT scores to compartmentalize model
[comp_model, gene_loc, transporters, scores, removed] = ...
    predictLocalization(model, gss, 'c', 0.5, 60 * 3, true);