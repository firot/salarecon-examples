atomicwrites==1.4.0
attrs==21.4.0
backcall==0.2.0
certifi==2020.6.20
charset-normalizer==2.0.10
cobra==0.17.1
colorama==0.4.4
cycler==0.11.0
decorator==4.4.2
depinfo==1.7.0
et-xmlfile==1.1.0
future==0.18.2
gprofiler==1.2.2
gurobipy==9.1.2
idna==3.3
importlib-metadata==4.8.3
iniconfig==1.1.1
ipykernel==5.3.4
ipython==7.16.1
ipython-genutils==0.2.0
itsdangerous==2.0.1
jedi==0.17.2
joblib==1.1.0
jupyter-client==6.1.7
jupyter-core==4.6.3
kiwisolver==1.3.1
matplotlib==3.3.4
mpmath==1.2.1
mptool==0.1.0
networkx==2.5.1
numpy==1.19.5
openpyxl==3.0.9
optlang==1.5.2
packaging==21.3
pandas==1.1.5
parso==0.7.0
pickleshare==0.7.5
Pillow==8.4.0
pip==20.2.4
pluggy==1.0.0
prompt-toolkit==3.0.8
py==1.11.0
pybiomart==0.2.0
Pygments==2.7.1
pyparsing==3.0.6
pytest==6.2.5
python-dateutil==2.8.2
python-libsbml-experimental==5.18.0
pytz==2021.3
pywin32==227
PyYAML==6.0
pyzmq==19.0.2
requests==2.27.1
requests-cache==0.7.5
ruamel.yaml==0.17.20
ruamel.yaml.clib==0.2.6
scikit-learn==0.24.2
scipy==1.5.4
seaborn==0.11.2
setuptools==50.3.0.post20201006
six==1.16.0
sklearn==0.0
swiglpk==5.0.4
sympy==1.9
threadpoolctl==3.0.0
toml==0.10.2
tornado==6.0.4
traitlets==4.3.3
typing-extensions==4.0.1
url-normalize==1.4.3
urllib3==1.26.8
wcwidth==0.2.5
wheel==0.35.1
wincertstore==0.2
zipp==3.6.0
