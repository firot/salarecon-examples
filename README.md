Special case of the SALARECON repo for the DLN course in Selbu, fall 2022.  
Most files intended for use is in the root directory.  
To run the salarecon_examples.ipynb following package installations is needed: 
```
mamba create salarecon
conda activate salarecon
mamba install pip
mamba install jupyterlab
pip install pandas
pip install cobra
pip install seaborn
pip install openpyxl
```